# deepin-dock-plugin-weather-git

A plugin for deepin dock, display weather information.

https://github.com/CareF/deepin-dock-plugin-weather

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-packages/deepin-dock-plugin-weather-git.git
```

